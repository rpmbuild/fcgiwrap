Name:           fcgiwrap
Version:        1.16.1
Release:        1%{?dist}
Summary:        Span fcgi is a common fastcgi management server

License:        GPL
Packager:	LiYang <285406724@qq.com>
URL:            www.test.com
Source0:        %{name}-%{version}.tar.gz

BuildRoot:      %_topdir/BUILDROOT
BuildRequires:  gcc
Requires:       gcc,spawn-fcgi,autoconf,automake,libtool,fcgi,fcgi-devel

%description
Span fcgi is a common fastcgi management server.


%prep
%setup -q


%build
autoreconf -i
./configure
make CFLAGS='-Wno-implicit-fallthrough' %{?_smp_mflags}


%install
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install
install -m 0755 -d $RPM_BUILD_ROOT/usr/lib/systemd/system/
install -D -m 644 $RPM_BUILD_DIR/%{name}-%{version}/systemd/fcgiwrap.service %{buildroot}/usr/lib/systemd/system/fcgiwrap.service
install -D -m 644 $RPM_BUILD_DIR/%{name}-%{version}/systemd/fcgiwrap.socket %{buildroot}/usr/lib/systemd/system/fcgiwrap.socket


%pre
egrep "^dspam" /etc/passwd >/dev/null
if [[ $? -eq 1 ]]; then
	/usr/sbin/groupadd dspam -g 104 -f  2> /dev/null
	/usr/sbin/useradd -u 103 -g 104 dspam -M -s /sbin/nologin -c "Mail system usage" 2> /dev/null
fi

%post
systemctl daemon-reload 2> /dev/null
systemctl start spawn-fcgi 2> /dev/null

%preun
systemctl stop spawn-fcgi
rm -fr /usr/local/sbin/fcgiwrap
rm -fr /usr/local/man/man8/fcgiwrap.8
rm -fr /usr/lib/systemd/system/fcgiwrap.service
rm -fr /usr/lib/systemd/system/fcgiwrap.socket

%clean
#[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf "$RPM_BUILD_ROOT"
#[ "$RPM_BUILD_DIR" != "/" ] && rm -rf $RPM_BUILD_DIR/*


%files
%defattr(-,root,root,0755)
%config(noreplace)   /usr/lib/systemd/system/fcgiwrap.service
%config(noreplace)   /usr/lib/systemd/system/fcgiwrap.socket
%config(noreplace)   /usr/local/man/man8/fcgiwrap.8
%config(noreplace)   /usr/local/sbin/fcgiwrap



%changelog
